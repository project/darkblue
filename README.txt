
AUTHOR
------------------
Theme ported to Drupal 6 by Manuel Garcia - http://drupal.org/user/213194/

A port from the free template Dark Blue from http://www.minimalistic-design.net
You can see the original demo here: http://www.minimalistic-design.net/newtemplates/2/

Released under GNU/GPL license for the drupal community with consent from the author. 
http://www.fsf.org/licensing/licenses/gpl.html


DESCRIPTION
-----------
A simple yet elegant theme, ideal for a blog.

Go to /admin/build/themes/settings/darkblue to configure the:
- Logo
- Site name
- Site slogan
- Footer message
- Shortcut icon
- Search box
- Primary Links
- Secondary Links
- Blog entries with fancy dates (taken from clean template) - for BLOG content types

Regions available for blocks:
- Content Top *
- Right sidebar
- Footer *

* Both regions "Content Top" and "Footer" are layed out to have the block's width be a third of the space available.
So, if you have 3 blocks they will display side by side properly; If you have more they will drop to the next line.
If you want to have only one block on the whole region, you will want to edit the layout.css and remove the width for the blocks in that region.

Contains three css files:
layout.css
style.css
fancy-dates.css 

Layout has been separated from the styling as much as posible, though some classes' styling-layout are kept in one place for comodity. Use firebug to ease your pain!


BROWSER COMPATIBILITY
---------------------
This theme has been tested on IE6, IE7 and Firefox. 
If you can, do test it on other browsers and post an issue if necessary.
